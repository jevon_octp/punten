import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:pw_eas_in/screen/satpam.dart';
import 'package:pw_eas_in/screen/teacher/details.dart';
import 'package:pw_eas_in/screen/teacher/scan.dart';
import 'package:toast/toast.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

class sign extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _sign();
}

class _sign extends State<sign>{
  String email;
  String hello;
  bool isLoading = false;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  PageController _pageController = PageController(initialPage: 1);

  void _handleSignIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    email = googleUser.email;

    if(email.contains('.smktelkom-mlg.sch.id') == true){
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      setState(() {
        isLoading = true;
      });
      final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
      print("signed in " + user.email);
    }

    else if(email.contains('coba') == true || email.contains('patriciadika31') == true || email.contains('jevonop88') == true){
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      setState(() {
        isLoading = true;
      });
      final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
      print("signed in " + user.email);
    }

    else{
      Toast.show(
          "gunakan akun sekolah",
          context,
          duration: Toast.LENGTH_LONG,
          gravity:  Toast.BOTTOM,
          textColor: Colors.white,
          backgroundColor: Colors.red
      );
      signOut();
    }
  }

  signOut() {
    _googleSignIn.signOut();
    _auth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _page = <Widget>
    [
      Center(),
      Center(
        child:
        isLoading == false ? RaisedButton(
          color: Colors.blue,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15)
          ),
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: Image.asset('assets/google.png',
                  height: 20,
                  width: 20,
                ),
              ),
              Text('Sign in with Google',
                  style: TextStyle(
                      color: Colors.white
                  )),
            ],
          ),
          onPressed: _handleSignIn,
        ) : CircularProgressIndicator(),
      ),
    ];

    return Scaffold(
      body: PageView(
        onPageChanged: (PageId){
          if (PageId == _page.length - 1) {
            setState(() {
              Future.delayed(const Duration(milliseconds: 500), () {
                _pageController.jumpToPage(1);
              });
            });
          }
          if (PageId == 0) {
            setState(() async {
              Future.delayed(const Duration(milliseconds: 500), () {
                _pageController.jumpToPage(1);
              });
              final barcode = await Navigator.of(context).push<Barcode>(
                MaterialPageRoute(builder: (c) {
                  return ScanPage();
                },
                ),
              );
              if (barcode == null) {
                return;
              }
              setState(() {
                hello = barcode.displayValue;
                print('data : $hello');
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => satpam(kode: hello)));
              });
            });
          }
        },
        controller: _pageController,
        children: _page,
      ),
    );
  }
  void _onHorizontalDrag(DragEndDetails details) {
    if(details.primaryVelocity == 0) return; // user have just tapped on screen (no dragging)

    if (details.primaryVelocity.compareTo(0) == -1)
      print('dragged from left');
    else
      print('dragged from right');
  }
}
