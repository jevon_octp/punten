import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter_camera_ml_vision/flutter_camera_ml_vision.dart';
import 'package:pw_eas_in/screen/ScannerOverlay.dart';

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  bool resultSent = false;
  List<String> data = [];
  var datauser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Stack(
        children: <Widget>[
          Positioned.fill(
            child: CameraMlVision<List<Barcode>>(
              overlayBuilder: (c) {
                return Container(
                  decoration: ShapeDecoration(
                    shape: ScannerOverlayShape(
                      borderColor: Theme.of(context).primaryColor,
                      borderWidth: 3.0,
                    ),
                  ),
                );
              },
              detector: FirebaseVision.instance.barcodeDetector().detectInImage,
              onResult: (List<Barcode> barcodes) {
                if (!mounted || resultSent) {
                  return;
                }
                resultSent = true;
                print(barcodes.first);
                Navigator.of(context).pop<Barcode>(barcodes.first);
                StreamBuilder<QuerySnapshot>(
                  stream: Firestore.instance.collection('perizinan').where('id', isEqualTo: barcodes.first).snapshots(),
                  builder: (context, snapshot){
                    datauser = snapshot.data.documents.map((data) {
                      return data.data;
                    }).toList();
                    if(datauser == null){
                      print('hello');
                      return Text('hello');
                    }
                    else{
                      print('hello');
                      return Text('OK');
                    }
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}