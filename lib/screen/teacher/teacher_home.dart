import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:pw_eas_in/screen/teacher/details.dart';
import 'package:pw_eas_in/screen/teacher/scan.dart';
import 'package:pw_eas_in/service/service.dart';

class teacher_home extends StatefulWidget{
  var data;
  teacher_home({this.data});
  @override
  State<StatefulWidget> createState() => _teacher_home();
}

class _teacher_home extends State<teacher_home>{
  PageController _pageController = PageController(initialPage: 1);
  ScrollController _scrollController = new ScrollController();
  DateTime tanggal;
  String pilihan;
  var hello;
  final db = Firestore.instance;
  var datauser;
  String tanggalFormated;
  bool jabatan =  false;
  String jabatanGuru;

  @override
  void initState() {
    super.initState();
    tanggal = DateTime.now();
    this.formatDate();
    pilihan = DateFormat('dd-MM-yyyy').format(tanggal);
  }

  void formatDate(){
    tanggalFormated = DateFormat('EEE, dd MMM yyyy').format(tanggal);
    setState((){});
  }

  Widget topContent() => Row(
    children: <Widget>[
      Expanded(
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 3),
                child: FlatButton(
                  child: Icon(Icons.arrow_left, color: Colors.white,),
                  onPressed: (){
                    setState(() {
                      tanggal = tanggal.add(new Duration(days: -1));
                      pilihan = DateFormat('dd-MM-yyyy').format(tanggal);
                      this.formatDate();
                      Future.delayed(const Duration(milliseconds: 350), ()
                      {
                        _pageController.jumpToPage(1);
                      });
                    });
                  },
                ),
              ),
              RaisedButton(
                color: Colors.white,
                child: Text(tanggalFormated, style: TextStyle(color: Colors.blue)),
                onPressed: ()async
                {
                  DateTime picked = await showDatePicker(
                    context: context,
                    initialDate: tanggal,
                    firstDate: DateTime(2010),
                    lastDate: DateTime(2040),
                  );
                  if(picked != null){
                    tanggal = picked;
                    this.formatDate();
                    pilihan = DateFormat('dd-MM-yyyy').format(tanggal);
                  }
                },
              ),
              Padding(
                padding: const EdgeInsets.only(left: 3),
                child: FlatButton(
                    child: Icon(Icons.arrow_right, color: Colors.white),
                    onPressed: (){
                      setState(() {
                        tanggal = tanggal.add(new Duration(days: 1));
                        pilihan = DateFormat('dd-MM-yyyy').format(tanggal);
                        this.formatDate();
                        Future.delayed(const Duration(milliseconds: 350), ()
                        {
                          _pageController.jumpToPage(1);
                        });
                      });
                    }
                ),
              ),
            ],
          ),
        ),
      ),
    ],
  );

  Widget buildItem(doc, int) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0)
      ),
      child: InkWell(
        onTap: ()
        {
          Navigator.push(context, MaterialPageRoute(
              builder: (context) =>
                  details(kode: doc[int]['id_kode'].toString(), admin: widget.data.displayName)));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 15, left: 15),
              child: Text(
                '${doc[int]['pembuat']}'.toUpperCase(),
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15,),
              child: Text(
                '${doc[int]['keperluan']}'.toUpperCase(),
                style: TextStyle(fontSize: 18 ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Text(
                '${doc[int]['jam_mulai']} - ${doc[int]['jam_selesai']}',
                style: TextStyle(fontSize: 15),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15, bottom: 15 ),
              child: textKu(int),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _page = <Widget>
    [
      Center(
        child: Text('Loading'),
      ),
      Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: db.collection('perizinan')
              .where('tanggal', isEqualTo: pilihan)
              .orderBy('jam_selesai', descending: false)
              .snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              datauser = snapshot.data.documents.map((data) {
                return data.data;
              }).toList();
              if (snapshot.data.documents.length == 0) {
                return Center(
                  child: Text('tidak ada data',
                      style: TextStyle(
                          color: Colors.black45
                      )),
                );
              }
              else {
                return ListView.builder(
                    padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                    itemCount: snapshot.data == null ? 0 : snapshot.data.documents.length,
                    itemBuilder: (BuildContext context, int index) {
                      if(datauser[index]['v_bk'] == null || datauser[index]['v_piket'] == null || datauser[index]['v_guru'] == null){
                        Timer(Duration(hours: 10),() => db.collection('perizinan')
                            .document(datauser[index]['id'])
                            .delete());
                      }
                      return buildItem(datauser, index);
                    }
                );
              }
            }
            else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
      Center(
          child: Text("Loading")
      ),
    ];
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(100),
          child: Stack(
            children: <Widget>[
              AppBar(
                title: Text('Dasboard'),
                centerTitle: true,
                actions: <Widget>[
                  IconButton(
                      icon: Icon(
                          Icons.exit_to_app,
                          color: Colors.white),
                      onPressed: () {
                        service().signOut();
                      }
                  )
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: topContent(),
                ),
              ),
            ],
          )
      ),
      body:
      Stack(
        children: <Widget>[
          PageView(
            onPageChanged: (pageID) {
              if (pageID == _page.length - 1) {
                setState(() {
                  tanggal = tanggal.add(Duration(days: 1));
                  pilihan = DateFormat('dd-MM-yyyy').format(tanggal);
                  Future.delayed(const Duration(milliseconds: 500), () {
                    _pageController.jumpToPage(1);
                  });
                });
              }
              if (pageID == 0) {
                setState(() {
                  tanggal = tanggal.add(new Duration(days: -1));
                  pilihan = DateFormat('dd-MM-yyyy').format(tanggal);
                  Future.delayed(const Duration(milliseconds: 500), () {
                    _pageController.jumpToPage(1);
                  });
                });
              }
              setState(() {});
              this.formatDate();
            },
            controller: _pageController,
            children: _page,
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                width: 55,
                height: 55,
                child: FittedBox(
                  child: FloatingActionButton(
                    backgroundColor: Colors.blue,
                    child: Icon(Icons.camera_alt),
                    onPressed: () async {
                      final barcode = await Navigator.of(context).push<Barcode>(
                        MaterialPageRoute(builder: (c) {
                          return ScanPage();
                        },
                        ),
                      );
                      if (barcode == null) {
                        return;
                      }
                      setState(() {
                        hello = barcode.displayValue;
                        print('data : $hello');
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => details(kode: hello, admin: widget.data.displayName)));
                      });
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Text textKu(int index){
    if(datauser[index]['v_piket'] != null){
      print('gurru');
      return Text(
          'Terverifikasi Guru Piket',
          style: TextStyle(
              fontSize: 20,
              color: Colors.green,
              fontWeight: FontWeight.bold
          )
      );
    }
    else if(datauser[index]['v_bk'] != null){
      print('kesis');
      return Text(
          'Terverifikasi Kesiswaan',
          style: TextStyle(
              fontSize: 20,
              color: Colors.green,
              fontWeight: FontWeight.bold
          )
      );
    }
    else if(datauser[index]['v_guru'] != null){
      print('piket');
      return Text(
          'Terverifikasi Guru Mapel ',
          style: TextStyle(
              fontSize: 20,
              color: Colors.green,
              fontWeight: FontWeight.bold
          )
      );
    }
    else{
      return Text(
          'Belum Terverifikasi',
          style: TextStyle(
              fontSize: 20,
              color: Colors.red,
              fontWeight: FontWeight.bold
          )
      );
    }
  }
}