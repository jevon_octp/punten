import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class details extends StatefulWidget{
  String kode;
  String admin;
  details({this.kode, this.admin});
  @override
  State<StatefulWidget> createState() => _details();
}

class _details extends State<details>{
  final db = Firestore.instance;
  var datauser;

  TextEditingController keteranganControl = TextEditingController();
  TextEditingController kesiswaanControl = TextEditingController();
  TextEditingController piketControl = TextEditingController();
  TextEditingController guruControl = TextEditingController();
  TextEditingController nomorControl = TextEditingController();
  TextEditingController namaControl = TextEditingController();
  TextEditingController startControl = TextEditingController();
  TextEditingController endControl = TextEditingController();

  String idAct;   String keperluan;
  String nama;    String start;
  String end;     String keterangan;
  String piket;   String kesiswaan;
  String guru;    String nomor;

  int mulai;
  int sampai;
  var coba;
  var jam1;     var menit1;
  var jam2;     var menit2;

  String _radioValue; //Initial definition of radio button value
  String choice;
  String _hello;

  bool isLoading = false;
  TimeOfDay timeStart;
  TimeOfDay timeEnd;
  int pilih = 0;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState(){
    var hello = widget.kode.substring(widget.kode.length - 1, widget.kode.length);
    if(int.parse(hello) == 1){
      _radioValue = "izin";
    }
    else{
      _radioValue = "sakit";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Details'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                  Icons.delete,
                  color: Colors.red),
              onPressed: () {
                show_Dialog1(datauser[0]['id']);
              }
          )
        ],
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: db.collection('perizinan')
              .where('id_kode', isEqualTo: widget.kode)
              .snapshots(),
          builder: (context, snapshot){
            if(snapshot.hasData){
                datauser = snapshot.data.documents.map((data){
                  return data.data ;
                }).toList();
              if(snapshot.data.documents.length == 0){
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 150,
                        height: 150,
                        child: Image.asset(
                          'assets/ops.png',
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Text(
                          'Kode Tidak Cocok',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45
                          ),
                        ),
                      ),
                    ],
                  )
                );
              }
              else{
                idAct = datauser[0]['id'];
                namaControl.text = datauser[0]['nama'];
                startControl.text = datauser[0]['jam_mulai'];
                endControl.text = datauser[0]['jam_selesai'];
                keteranganControl.text = datauser[0]['keterangan'];
                kesiswaanControl.text = datauser[0]['v_bk'];
                guruControl.text = datauser[0]['v_guru'];
                piketControl.text = datauser[0]['v_piket'];
                nomorControl.text = datauser[0]['telp'];
                _hello = "${datauser[0]['keperluan']}";
                start = datauser[0]['jam_mulai'];
                end = datauser[0]['jam_selesai'];
                timeStart = TimeOfDay(hour:int.parse(start.split(".")[0]),minute: int.parse(start.split(".")[1]));
                timeEnd = TimeOfDay(hour:int.parse(end.split(".")[0]),minute: int.parse(end.split(".")[1]));
                jam1 = timeStart.hour.toString().padLeft(2, '0');
                menit1 = timeStart.minute.toString().padLeft(2, '0');
                mulai = int.parse(jam1) * 60 + int.parse(menit1);
                jam2 = timeEnd.hour.toString().padLeft(2, '0');
                menit2 = timeEnd.minute.toString().padLeft(2, '0');
                sampai = int.parse(jam2) * 60 + int.parse(menit2);
                return ListView(
                  padding: const EdgeInsets.only(bottom: 10),
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 15),
                      child: Text(
                        'Data Utama',
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.blue
                        ),
                      ),
                    ),
                    Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                              child: Container(
                                decoration: new BoxDecoration(
                                    color: Colors.black12,
                                    borderRadius: new BorderRadius.only(
                                        topLeft:  const  Radius.circular(8.0),
                                        topRight: const  Radius.circular(8.0))
                                ),
                                child: TextFormField(
                                  controller: namaControl,
                                  style: TextStyle(fontSize: 20),
                                  decoration: InputDecoration(
                                    labelText: 'Nama Siswa',
                                    hintStyle: TextStyle(fontSize: 20),
                                    contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return '';
                                    }
                                  },
                                  onSaved: (value) => nama = value,
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: ()
                              {
                                TimePicker(context);
                              },
                              child: AbsorbPointer(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                                  child: Container(
                                    decoration: new BoxDecoration(
                                        color: Colors.black12,
                                        borderRadius: new BorderRadius.only(
                                            topLeft:  const  Radius.circular(8.0),
                                            topRight: const  Radius.circular(8.0))
                                    ),
                                    child: TextFormField(
                                      controller: startControl,
                                      style: TextStyle(fontSize: 20),
                                      decoration: InputDecoration(
                                        labelText: 'Jam Mulai',
                                        hintStyle: TextStyle(fontSize: 20),
                                        contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                      ),
                                      onSaved: (value) => start = value,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: ()
                              {
                                TimePicker2(context);
                              },
                              child: AbsorbPointer(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                                  child: Container(
                                    decoration: new BoxDecoration(
                                        color: Colors.black12,
                                        borderRadius: new BorderRadius.only(
                                            topLeft:  const  Radius.circular(8.0),
                                            topRight: const  Radius.circular(8.0))
                                    ),
                                    child: TextFormField(
                                      controller: endControl,
                                      style: TextStyle(fontSize: 20),
                                      decoration: InputDecoration(
                                        labelText: 'Jam Selesai',
                                        hintStyle: TextStyle(fontSize: 20),
                                        contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                      ),
                                      onSaved: (value) => end = value,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10, left: 15),
                              child: Text(
                                'Data Tambahan',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.blue
                                ),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                                    child: Container(
                                      decoration: new BoxDecoration(
                                          color: Colors.black12,
                                          borderRadius: new BorderRadius.only(
                                              topLeft:  const  Radius.circular(8.0),
                                              topRight: const  Radius.circular(8.0))
                                      ),
                                      child: TextFormField(
                                        controller: nomorControl,
                                        keyboardType: TextInputType.phone,
                                        style: TextStyle(fontSize: 20),
                                        decoration: InputDecoration(
                                          labelText: 'Nomor Telp',
                                          hintText: '082232620434',
                                          hintStyle: TextStyle(fontSize: 20),
                                          contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                        ),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return '';
                                          }
                                        },
                                        onSaved: (value) => nomor = value,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 15, right: 15),
                                  child: Container(
                                    width: 50,
                                    height: 50,
                                    child: FittedBox(
                                      child: FloatingActionButton(
                                        child: Icon(Icons.phone),
                                        onPressed: (){
                                          showDialogcall(namaControl.text, nomorControl.text);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10, left: 15),
                              child: Text(
                                'Keperluan',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.blue
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Radio(
                                    value: 'izin',
                                    groupValue: _radioValue,
                                    onChanged: radioButtonChanges,
                                  ),
                                  Text(
                                    "Izin",
                                  ),
                                  Radio(
                                    value: 'sakit',
                                    groupValue: _radioValue,
                                    onChanged: radioButtonChanges,
                                  ),
                                  Text(
                                    "Sakit",
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10, left: 15),
                              child: Text(
                                'Keterangan ',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.blue
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                              child: Container(
                                decoration: new BoxDecoration(
                                    color: Colors.black12,
                                    borderRadius: new BorderRadius.only(
                                        topLeft:  const  Radius.circular(8.0),
                                        topRight: const  Radius.circular(8.0))
                                ),
                                child: TextFormField(
                                  controller: keteranganControl,
                                  style: TextStyle(fontSize: 20),
                                  decoration: InputDecoration(
                                    labelText: 'Keterangan',
                                    hintStyle: TextStyle(fontSize: 20),
                                    contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return '';
                                    }
                                  },
                                  onSaved: (value) => keterangan = value,
                                ),
                              ),
                            ),
                            datauser[0]['v_piket'] != null || datauser[0]['v_guru'] != null || datauser[0]['v_bk'] != null ?
                            Padding(
                              padding: const EdgeInsets.only(top: 10, left: 15),
                              child: Text(
                                'Terferifikasi',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.green
                                ),
                              ),
                            ) : SizedBox(),
                            datauser[0]['v_guru'] != null ?
                            Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                              child: Container(
                                decoration: new BoxDecoration(
                                    color: Colors.black12,
                                    borderRadius: new BorderRadius.only(
                                        topLeft:  const  Radius.circular(8.0),
                                        topRight: const  Radius.circular(8.0))
                                ),
                                child: TextFormField(
                                  enabled: false,
                                  controller: guruControl,
                                  enableInteractiveSelection: false,
                                  style: TextStyle(fontSize: 20),
                                  decoration: InputDecoration(
                                    labelText: 'Guru Mapel',
                                    hintStyle: TextStyle(fontSize: 20),
                                    contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return '';
                                    }
                                  },
                                  onSaved: (value) => guru = value,
                                ),
                              ),
                            ): SizedBox(),
                            datauser[0]['v_bk'] != null ?
                            Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                              child: Container(
                                decoration: new BoxDecoration(
                                    color: Colors.black12,
                                    borderRadius: new BorderRadius.only(
                                        topLeft:  const  Radius.circular(8.0),
                                        topRight: const  Radius.circular(8.0))
                                ),
                                child: TextFormField(
                                  enabled: false,
                                  controller: kesiswaanControl,
                                  enableInteractiveSelection: false,
                                  style: TextStyle(fontSize: 20),
                                  decoration: InputDecoration(
                                    labelText: 'Kesiswaan',
                                    hintStyle: TextStyle(fontSize: 20),
                                    contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return '';
                                    }
                                  },
                                  onSaved: (value) => kesiswaan = value,
                                ),
                              ),
                            ): SizedBox(),
                            datauser[0]['v_piket'] != null ?
                            Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                              child: Container(
                                decoration: new BoxDecoration(
                                    color: Colors.black12,
                                    borderRadius: new BorderRadius.only(
                                        topLeft:  const  Radius.circular(8.0),
                                        topRight: const  Radius.circular(8.0))
                                ),
                                child: TextFormField(
                                  enabled: false,
                                  controller: piketControl,
                                  enableInteractiveSelection: false,
                                  style: TextStyle(fontSize: 20),
                                  decoration: InputDecoration(
                                    labelText: 'Guru Piket',
                                    hintStyle: TextStyle(fontSize: 20),
                                    contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return '';
                                    }
                                  },
                                  onSaved: (value) => piket = value,
                                ),
                              ),
                            ): SizedBox(),
                          ],
                        )),
                    datauser[0]['v_piket'] == null || datauser[0]['v_guru'] == null || datauser[0]['v_bk'] == null ?
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: isLoading == false ? RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18),
                          ),
                          color: Colors.blue,
                          padding: const EdgeInsets.all(15),
                          child: Text(
                              'Verifikasi',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white
                              )),
                          onPressed: (){
                            if(widget.admin == datauser[0]['v_piket'] || widget.admin == datauser[0]['v_guru'] || widget.admin == datauser[0]['v_bk']){
                              Toast.show(
                                  "Anda tidak dapat melakukan verifikasi",
                                  context,
                                  duration: Toast.LENGTH_LONG,
                                  gravity:  Toast.BOTTOM,
                                  textColor: Colors.white,
                                  backgroundColor: Colors.red
                              );
                            }
                            else{
                              show_Dialog();
                            }
                          }
                      ) : Center(child: CircularProgressIndicator()),
                    ): SizedBox()
                  ],
                );
              }
            }
            else{
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }

  void show_Dialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Center(child: Text("Verifikasi",
            style: TextStyle(
                color: Colors.blue,
                fontWeight: FontWeight.bold,
            ),
          )),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              datauser[0]['v_guru'] == null ?
              RaisedButton(
                color: Colors.blue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18)
                ),
                padding: const EdgeInsets.all(13),
                child: Text('Guru Mapel',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                    )),
                onPressed: ()async{
                  await UpdateData('v_guru', widget.admin);
                },
              ):SizedBox(),
              Padding(
                padding: const EdgeInsets.only(top : 8.0),
                child: datauser[0]['v_bk'] == null ?
                RaisedButton(
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18)
                  ),
                  padding: const EdgeInsets.all(13),
                  child: Text('Kesiswaan',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                      )),
                  onPressed: ()async{
                    await UpdateData('v_bk', widget.admin);
                  },
                ):SizedBox(),
              ),
              datauser[0]['v_piket'] == null ?
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: RaisedButton(
                  color: Colors.orange,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18)
                  ),
                  padding: const EdgeInsets.all(13),
                  child: Text('Guru Piket',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                      )),
                  onPressed: ()async{
                    await UpdateData('v_piket', widget.admin);
                  },
                ),
              ): SizedBox(),
            ],
          ),
        );
      },
    );
  }

  void popUp(){
    Toast.show(
        "Berhasil Terverifikasi",
        context,
        duration: Toast.LENGTH_LONG,
        gravity:  Toast.BOTTOM,
        textColor: Colors.white,
        backgroundColor: Colors.green
    );
  }

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'sakit':
          choice = value;
          break;
        case 'izin':
          choice = value;
          break;
        default:
          choice = null;
      }
      debugPrint(choice); //Debug the choice in console
    });
  }

  void UpdateData(String field, String value) async{
    if(namaControl.text != '' && startControl.text != '' && endControl.text != '' && nomorControl.text != '' && keteranganControl.text != ''){
      if(mulai < sampai){
        _formKey.currentState.save();
        await db.collection('perizinan')
            .document(datauser[0]['id'])
            .updateData({
          'nama'        : namaControl.text,
          'keperluan'   : _radioValue,
          'keterangan'  : keteranganControl.text,
          'telp'        : nomorControl.text,  
          'jam_mulai'   : start,
          'jam_selesai' : end,
          '$field'      : value,
        });
        FocusScope.of(context).requestFocus(new FocusNode());
        Navigator.of(context).pop();
        popUp();
      }
      else{
        Toast.show(
            "Jam tidak valid",
            context,
            duration: Toast.LENGTH_SHORT,
            gravity:  Toast.BOTTOM,
            textColor: Colors.white,
            backgroundColor: Colors.red
        );
      }
    }
    else{
      Toast.show(
          "isi form di atas",
          context,
          duration: Toast.LENGTH_SHORT,
          gravity:  Toast.BOTTOM,
          textColor: Colors.white,
          backgroundColor: Colors.red
      );
    }
  }

  Future<Null> TimePicker(BuildContext context)async{
    var jam;
    var menit;
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: timeStart,
    );
    if(picked != null){
      timeStart = picked;
      jam = timeStart.hour.toString().padLeft(2, '0');
      menit = timeStart.minute.toString().padLeft(2, '0');
      startControl.text = '$jam.$menit';
      mulai = int.parse(jam) * 60 + int.parse(menit);
    }
  }

  Future<Null> TimePicker2(BuildContext context)async{
    var jam;
    var menit;
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: timeEnd,
    );
    if(picked != null){
      timeEnd = picked;
      jam = timeEnd.hour.toString().padLeft(2, '0');
      menit = timeEnd.minute.toString().padLeft(2, '0');
      endControl.text = '$jam.$menit';
      sampai = int.parse(jam) * 60 + int.parse(menit);
    }
  }

  void showDialogcall(String name, String phone){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Center(
                child: Text(name,style: TextStyle(
                    fontWeight: FontWeight.bold
                ))
            ),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                    child: Text("Phone", style: TextStyle(color: Colors.white)),
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15))
                    ),
                    onPressed: () {
                      _dialPhone(phone);
                    }
                ),
                RaisedButton(
                    child: Text("Whatsapp", style: TextStyle(color: Colors.white)),
                    color: Colors.green,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15))
                    ),
                    onPressed: (){
                     _whatsapp(phone);
                    }
                ),
              ],
            )
        );
      },
    );
  }
  void _whatsapp(String phone) async {
    String url = "https://api.whatsapp.com/send?phone=$phone";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _dialPhone(String phone) async {
    String url = "tel:$phone";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  void show_Dialog1(String id) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Center(child: Text("Anda yakin ?",
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Anda tidak dapat memulihkan data anda lagi',
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      child: new Text("batal", style: TextStyle(
                          color: Colors.white)),
                      color: Colors.grey,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    RaisedButton(
                      child: Text("Ya, hapus data", style: TextStyle(
                          color: Colors.white)),
                      color: Colors.red,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))
                      ),
                      onPressed: (){
                        db.collection('perizinan')
                            .document(id)
                            .delete();
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
