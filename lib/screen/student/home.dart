import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pw_eas_in/screen/student/permit.dart';
import 'package:pw_eas_in/screen/student/show.dart';
import 'package:pw_eas_in/service/service.dart';

class home extends StatefulWidget{
  var data;
  home({this.data});
  @override
  State<StatefulWidget> createState() => _home();
}

class _home extends State<home>{

  final db = Firestore.instance;
  DocumentReference ref;
  DateTime tanggal;
  bool validation = true;
  String harini;
  var datauser;

  @override
  void initState() {
    super.initState();
    print(widget.data);
    tanggal = DateTime.now();
    harini = DateFormat('dd-MM-yyyy').format(tanggal);
    print(harini);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Home'),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
                icon: Icon(
                    Icons.exit_to_app,
                    color: Colors.white),
                onPressed: (){
                  service().signOut();
                }
            )
          ],
        ),
        floatingActionButton:  FloatingActionButton(
          backgroundColor: Colors.blue,
          child: Icon(Icons.add),
          onPressed: (){
            if(validation == false){
              show_Dialog();
            }
            else{
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  permit(
                    hariIni: harini,
                    idUser: widget.data.uid,
                    username: widget.data.displayName,
                  )));
            }
          },
        ),
        body: Center(
          child: StreamBuilder<QuerySnapshot>(
            stream: db.collection('perizinan')
                .where('id_user', isEqualTo: widget.data.uid)
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                datauser = snapshot.data.documents.map((data) {
                  return data.data;
                }).toList();
                if (snapshot.data.documents.length == 0) {
                  return Center(
                    child: Text('tidak ada data',
                        style: TextStyle(
                            color: Colors.black45
                        )),
                  );
                }
                else {
                  return ListView.builder(
                      padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                      itemCount: snapshot.data == null ? 0 : snapshot.data.documents.length,
                      itemBuilder: (BuildContext context, int index) {
                        return buildItem(datauser, index);
                      }
                  );
                }
              }
              else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ),
    );
  }

  Widget buildItem(doc, int) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0)
      ),
      child: InkWell(
        onTap: ()
        {
          Navigator.push(context, MaterialPageRoute(builder: (context) => 
            show(
                  kode: doc[int]['id_kode'], 
                  tanggal: doc[int]['tanggal'],)
          ));
        },
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  '${doc[int]['nama']}'.toUpperCase(),
                  style: TextStyle(fontSize: 22),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5, top: 5),
                child: Text(
                  '${doc[int]['jam_mulai']} - ${doc[int]['jam_selesai']}',
                  style: TextStyle(fontSize: 18),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  '${doc[int]['keperluan']}'.toUpperCase(),
                  style: TextStyle(fontSize: 15),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child:
                datauser[int]['v_bk'] != null && datauser[int]['v_guru'] != null && datauser[int]['v_piket'] != null ?
                Text(
                  'Terverifikasi',
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                  ),
                ):
                RaisedButton(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)
                  ),
                  padding: const EdgeInsets.all(10),
                  child: Text('Hapus Perizinan', style: TextStyle(
                    color: Colors.white
                  )),
                  onPressed: (){
                    show_Dialog1(datauser[int]['id']);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  void show_Dialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Center(child: Text("Peringatan",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.red
            ),
          )),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                  'Anda tidak dapat menambah perizinan baru sebelum perizinan anda terverifikasi',
                  textAlign: TextAlign.center
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      child: new Text("Ok", style: TextStyle(
                          color: Colors.white)),
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
  void show_Dialog1(String id) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Center(child: Text("Anda yakin ?",
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Anda tidak dapat memulihkan data anda lagi',
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      child: new Text("batal", style: TextStyle(
                          color: Colors.white)),
                      color: Colors.grey,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    RaisedButton(
                      child: Text("Ya, hapus data", style: TextStyle(
                          color: Colors.white)),
                      color: Colors.red,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))
                      ),
                      onPressed: (){
                        db.collection('perizinan')
                            .document(id)
                            .delete();
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}