import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class show extends StatefulWidget{
  String kode;
  String tanggal;
  show({this.kode, this.tanggal});
  @override
  State<StatefulWidget> createState() => _show();
}

class _show extends State<show>{
  final db = Firestore.instance;
  var datauser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.kode);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Kode'),
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: db.collection('perizinan')
            .where('id_kode', isEqualTo: widget.kode)
            .snapshots(),
        builder: (context, snapshot){
          if(snapshot.hasData){
            datauser = snapshot.data.documents.map((data){
              return data.data;
            }).toList();
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: Container(
                    child: QrImage(
                      data: datauser[0]['id_kode'].toString(),
                      foregroundColor: Color(0xff03291c),
                    ),
                  ),
                ),
                Text(
                  datauser[0]['tanggal'],
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.blue,
                        fontWeight: FontWeight.bold
                    )),
                hello(),
              ],
            );
          }
          else{
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Text hello(){
    if(datauser[0]['v_guru'] == null){
      return Text(
          'Silahkan Menuju ke Guru Mapel',
          style: TextStyle(
              fontSize: 20,
              color: Colors.red,
              fontWeight: FontWeight.bold
          )
      );
    }
    else if(datauser[0]['v_bk'] == null){
      return Text(
          'Silahkan Menuju ke Kesiswaan',
          style: TextStyle(
              fontSize: 20,
              color: Colors.red,
              fontWeight: FontWeight.bold
          )
      );
    }
    else if(datauser[0]['v_piket'] == null){
      return Text(
          'Silahkan Menuju ke Guru Piket',
          style: TextStyle(
              fontSize: 20,
              color: Colors.red,
              fontWeight: FontWeight.bold
          )
      );
    }
    else{
      return Text(
          'Terverifikasi',
          style: TextStyle(
              fontSize: 20,
              color: Colors.green,
              fontWeight: FontWeight.bold
          )
      );
    }
  }
}