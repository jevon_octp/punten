import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crclib/crclib.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'dart:convert' show utf8;

class permit extends StatefulWidget{
  String hariIni;
  String idUser;
  String username;
  permit({this.hariIni, this.idUser, this.username});
  @override
  State<StatefulWidget> createState() => _permit();
}

class _permit extends State<permit>{
  TextEditingController namaControl = TextEditingController();
  TextEditingController startControl = TextEditingController();
  TextEditingController endControl = TextEditingController();
   TextEditingController keteranganControl = TextEditingController();
  TextEditingController nomorControl = TextEditingController();
  final db = Firestore.instance;
  DocumentReference ref;

  String idAct;
  String keperluan;
  String nama;
  String start;
  String end;
  String keterangan;
  String nomor;
  int mulai;
  int sampai;

  String _radioValue; //Initial definition of radio button value
  String choice;

  bool isLoading = false;
  TimeOfDay waktu;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    print(widget.username);
    waktu = TimeOfDay.now();
    setState(() {
      _radioValue = "izin";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Perizinan'),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 15),
            child: Text(
              'Data Utama',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.blue
              ),
            ),
          ),
          Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  buildTextFormField(),
                  Padding(
                    padding: const EdgeInsets.only(top: 10, left: 15),
                    child: Text(
                      'Data Tambahan',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.blue
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                    child: Container(
                      decoration: new BoxDecoration(
                          color: Colors.black12,
                          borderRadius: new BorderRadius.only(
                              topLeft:  const  Radius.circular(8.0),
                              topRight: const  Radius.circular(8.0))
                      ),
                      child: TextFormField(
                        controller: nomorControl,
                        keyboardType: TextInputType.phone,
                        enableInteractiveSelection: false,
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                          labelText: 'Nomor Telp',
                          hintText: '+6282232620434',
                          hintStyle: TextStyle(fontSize: 20),
                          contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return '';
                          }
                        },
                        onSaved: (value) => nomor = value,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10, left: 15),
                    child: Text(
                      'Keperluan',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.blue
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Radio(
                          value: 'izin',
                          groupValue: _radioValue,
                          onChanged: radioButtonChanges,
                        ),
                        Text(
                          "Izin",
                        ),
                        Radio(
                          value: 'sakit',
                          groupValue: _radioValue,
                          onChanged: radioButtonChanges,
                        ),
                        Text(
                          "Sakit",
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10, left: 15),
                    child: Text(
                      'Keterangan',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.blue
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                    child: Container(
                      decoration: new BoxDecoration(
                          color: Colors.black12,
                          borderRadius: new BorderRadius.only(
                              topLeft:  const  Radius.circular(8.0),
                              topRight: const  Radius.circular(8.0))
                      ),
                      child: TextFormField(
                        controller: keteranganControl,
                        enableInteractiveSelection: false,
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                         labelText: 'Keterangan',
                          hintStyle: TextStyle(fontSize: 20),
                          contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return '';
                          }
                        },
                        onSaved: (value) => keterangan = value,
                      ),
                    ),
                  ),
                ],
              )),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: isLoading == false ? RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
                color: Colors.blue,
                padding: const EdgeInsets.all(15),
                child: Text(
                    'Submit',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white
                    )),
                onPressed: addData
            ) : Center(child: CircularProgressIndicator()),
          )
        ],
      ),
    );
  }

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'izin':
          choice = value;
          break;
        case 'sakit':
          choice = value;
          break;
        default:
          choice = null;
      }
      print(choice); //Debug the choice in console
    });
  }

  Widget buildTextFormField() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
          child: Container(
            decoration: new BoxDecoration(
                color: Colors.black12,
                borderRadius: new BorderRadius.only(
                    topLeft:  const  Radius.circular(8.0),
                    topRight: const  Radius.circular(8.0))
            ),
            child: TextFormField(
              controller: namaControl,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              enableInteractiveSelection: false,
              style: TextStyle(fontSize: 20),
              decoration: InputDecoration(
                labelText: 'Nama Siswa',
                hintStyle: TextStyle(fontSize: 20),
                contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return '';
                }
              },
              onSaved: (value) => nama = value,
            ),
          ),
        ),
        GestureDetector(
          onTap: ()
          {
            TimePicker(context);
          },
          child: AbsorbPointer(
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
              child: Container(
                decoration: new BoxDecoration(
                    color: Colors.black12,
                    borderRadius: new BorderRadius.only(
                        topLeft:  const  Radius.circular(8.0),
                        topRight: const  Radius.circular(8.0))
                ),
                child: TextFormField(
                  controller: startControl,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    labelText: 'Jam Mulai',
                    hintStyle: TextStyle(fontSize: 20),
                    contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                  ),
                  onSaved: (value) => start = value,
                ),
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: ()
          {
            TimePicker2(context);
          },
          child: AbsorbPointer(
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
              child: Container(
                decoration: new BoxDecoration(
                    color: Colors.black12,
                    borderRadius: new BorderRadius.only(
                        topLeft:  const  Radius.circular(8.0),
                        topRight: const  Radius.circular(8.0))
                ),
                child: TextFormField(
                  controller: endControl,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    labelText: 'Jam Selesai',
                    hintStyle: TextStyle(fontSize: 20),
                    contentPadding: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 5),
                  ),
                  onSaved: (value) => end = value,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void addData()async{
    if(namaControl.text != '' && startControl.text != '' && endControl.text != '' && nomorControl.text != '' && keteranganControl.text != ''){
      if(mulai < sampai){
        setState(() {
          isLoading = true;
        });
        _formKey.currentState.save();
        ref = await db.collection('perizinan')
            .add({
          'nama'        : nama,
          'keperluan'   : _radioValue,
          'keterangan'  : keterangan,
          'pembuat'     : widget.username,
          'jam_mulai'   : start,
          'jam_selesai' : end,
          'tanggal'     : widget.hariIni,
          'telp'        : nomor,
          'id_user'     : widget.idUser,
          'v_piket'     : null,
          'v_guru'      : null,
          'v_bk'        : null
        });
        idAct = ref.documentID;
        var hello = Crc32Zlib().convert(utf8.encode(idAct));
        if(_radioValue == 'izin'){
          await db.collection('perizinan')
              .document(idAct)
              .updateData({
            'id' : idAct,
            'id_kode' : hello.toString() + '1'
          });
        }
        else{
          await db.collection('perizinan')
              .document(idAct)
              .updateData({
            'id' : idAct,
            'id_kode' : hello.toString() + '2'
          });
        }
        FocusScope.of(context).requestFocus(new FocusNode());
        Navigator.of(context).pop();
      }
      else{
        Toast.show(
            "Jam tidak valid",
            context,
            duration: Toast.LENGTH_SHORT,
            gravity:  Toast.BOTTOM,
            textColor: Colors.white,
            backgroundColor: Colors.red
        );
      }
    }
    else{
      Toast.show(
          "isi form di atas",
          context,
          duration: Toast.LENGTH_SHORT,
          gravity:  Toast.BOTTOM,
          textColor: Colors.white,
          backgroundColor: Colors.red
      );
    }
  }
  Future<Null> TimePicker(BuildContext context)async{
    var jam;
    var menit;
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: waktu,
    );
    if(picked != null){
      waktu = picked;
      jam = waktu.hour.toString().padLeft(2, '0');
      menit = waktu.minute.toString().padLeft(2, '0');
      startControl.text = '$jam.$menit';
      mulai = int.parse(jam) * 60 + int.parse(menit);
    }
  }

  Future<Null> TimePicker2(BuildContext context)async{
    var jam;
    var menit;
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: waktu,
    );
    if(picked != null){
      waktu = picked;
      print(waktu.hour);
      jam = waktu.hour.toString().padLeft(2, '0');
      menit = waktu.minute.toString().padLeft(2, '0');
      endControl.text = '$jam.$menit';
      sampai = int.parse(jam) * 60 + int.parse(menit);
    }
  }
}