import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/animation.dart';
import 'package:pw_eas_in/service/service.dart';

class splash extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _splash();
}

class _splash extends State<splash> with TickerProviderStateMixin{

  AnimationController _animationController;
  Animation<double> animation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 2000), vsync: this
    );
    animation = CurvedAnimation(parent: _animationController, curve: Curves.easeIn);
    _animationController.forward();
    Timer(Duration(seconds: 3),() =>
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => service().handleAuth())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FadeTransition(
          opacity: animation,
          child: Image.asset('assets/punten.png', height: 200, width: 200),
        ),
      ),
    );
  }
}