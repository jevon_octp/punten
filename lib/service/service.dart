import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:pw_eas_in/screen/sign.dart';
import 'package:pw_eas_in/screen/student/home.dart';
import 'package:pw_eas_in/screen/teacher/teacher_home.dart';

class service{
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  String email;
  Widget handleAuth(){
    return StreamBuilder(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshots)
      {
        if(snapshots.hasData){
          email = snapshots.data.email;
          if(email.contains('student') == true){
            return home(data: snapshots.data);
          }
          else{
            return teacher_home(data: snapshots.data);}
        }
        return sign();
      },
    );
  }

  signOut() {
    _googleSignIn.signOut();
    FirebaseAuth.instance.signOut();
  }
}